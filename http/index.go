package http

import (
	"net/http"
)

func (a *APIHandler) handleIndex(w http.ResponseWriter, _ *http.Request) {
	sendOK(w)
}
