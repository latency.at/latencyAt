package http

import (
	"time"

	"gitlab.com/latency.at/latencyAt"
)

func (a *APIHandler) createEmailToken() (*latencyAt.EmailToken, error) {
	t, err := genToken(a.Config.EmailTokenLength)
	if err != nil {
		return nil, err
	}
	emailToken := latencyAt.EmailToken{Token: t}
	if err := a.EmailTokenService.CreateEmailToken(&emailToken); err != nil {
		return nil, err
	}
	return &emailToken, nil
}

func (a *APIHandler) resetEmailToken(id int) (*latencyAt.EmailToken, error) {
	token, err := a.EmailTokenService.EmailToken(id)
	if err != nil {
		return nil, err
	}
	t, err := genToken(a.Config.EmailTokenLength)
	if err != nil {
		return nil, err
	}
	token.Token = t
	token.Created = time.Now()
	return token, a.EmailTokenService.UpdateEmailToken(token)
}
