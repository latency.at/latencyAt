package http

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"net/http"
	"net/mail"

	"gitlab.com/latency.at/latencyAt"
	"gitlab.com/latency.at/latencyAt/errors"
)

// FIXME: While this works and should be secure, there are probably better ways
func genToken(n int) (string, error) {
	btoken := make([]byte, n)
	_, err := rand.Read(btoken)
	if err != nil {
		return "", err
	}
	return base64.URLEncoding.EncodeToString(btoken)[:n], nil
}

func sendOK(w http.ResponseWriter) {
	err := json.NewEncoder(w).Encode(&latencyAt.Status{Status: "ok"})
	if err != nil {
		http.Error(w, errors.ErrInternal.Error(), http.StatusInternalServerError)
	}
}
func validate(email, password string) error {
	for _, err := range []error{
		validateEmail(email),
		validatePassword(password),
	} {
		if err != nil {
			return err
		}
	}
	return nil
}

func validateEmail(email string) error {
	_, err := mail.ParseAddress(email)
	return err
}

func validatePassword(password string) error {
	if len(password) < 7 {
		return errors.ErrPasswordShort
	}
	return nil
}
