# latency.at
![infra](infra.png)

*(Read-replica doesn't exist yet. Currently API is used for validating
requests.)*

- Running satellites across cloud providers and regions
- Multi-tenancy

## Domain structure
- latency.at; web served via CloudFlare
- api.latency.at; api
- `[region][index].[cloud-provider].mon.latency.at`; proxy

## Components
- `latencyAt` Backend code base, containing:
  - `api` api server
  - `balancer` to reset free account balance each month
  - `booker` receives pub/sub messages and decrements balance
- [web](https://gitlab.com/latency.at/web) react webapp, served by API server

## Stripe Integration
- Payments driven by webhooks to ensure consistency
- Require webhook to /api/hooks/stripe with following events enable:
  - `customer.subscription.updated` to create invoices on upgrades
  - `invoice.created` to bill users asap, which triggers...
  - `invoice.playment_succeeded`, that in turn actually updates balances
- TODO: set balance to zero one month after last payment
