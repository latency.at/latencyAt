package latencyAt

import (
	"encoding/json"
	"io"
	"time"
)

const DefaultBalance = 50000

type User struct {
	ID               int        `json:"-"`
	Email            string     `json:"email"`
	EmailTokenID     *int       `json:"-"`
	Password         string     `json:"password"`
	PasswordHash     []byte     `json:"-"`
	Activated        bool       `json:"activated"`
	Registered       time.Time  `json:"registered"`
	StripeCustomerID string     `json:"stripe_customer_id"`
	Balance          int        `json:"balance"`
	RequestsMonth    int        `json:"requests"`
	LastBilling      *time.Time `json:"last_billing"`
	VatIn            string     `json:"vatin"`
}

func UserFromJSON(r io.Reader) (*User, error) {
	user := User{}
	return &user, json.NewDecoder(r).Decode(&user)
}

type UserService interface {
	Healthy() error

	User(id int) (*User, error)
	UserByEmail(email string) (*User, error)
	UserByEmailTokenID(id int) (*User, error)
	UserByStripeCustomerID(id string) (*User, error)
	Create(u *User) error

	Activate(u *User) error
	SetPasswordHash(id int, passwordHash []byte) error
	SetEmailAndDeactivate(id int, email string) error

	SetStripeCustomerID(id int, customerID string) error
	SetVatIn(uid int, vatIn string) error
	Balance(u *User, n int) error
	BalanceAbs(u *User, n int) error
	BalanceByStripeCustomerID(id string, balance int) error
	BalanceAbsByStripeCustomerID(id string, balance int) error
	IncRequests(id, n int) error
	Purge() error
}
