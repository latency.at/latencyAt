package latencyAt

type MailService interface {
	Healthy() error

	SendActivation(u *User, t *EmailToken) error
	SendReset(u *User, t *EmailToken) error
}
