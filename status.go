package latencyAt

var (
	StatusOK = &Status{"OK", "Success"}
)

type Status struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}
