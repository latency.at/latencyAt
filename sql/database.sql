DROP DATABASE IF EXISTS latencyat;
DROP ROLE     IF EXISTS latencyat;

CREATE ROLE      latencyat LOGIN PASSWORD 'foobar23';
CREATE DATABASE  latencyat OWNER latencyat;
GRANT ALL PRIVILEGES ON DATABASE latencyat TO latencyat;
